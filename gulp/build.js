const ts = require('gulp-typescript');
const path = require('path');
const gulp = require('gulp');

gulp.task('build', () => {
  console.log(`${path.join(__dirname, '../*.ts')}`);
  return gulp.src(path.join(__dirname, '../*.ts'))
    .pipe(ts({ 
      noImplicitAny: false,
      target: "es6",
      declaration: false,
      sourceMap: true,
      module: "commonjs",
      moduleResolution: "node",
      noImplicitAny: false,
      preserveConstEnums: true,
      removeComments: true
    })).pipe(
      gulp.dest(path.join(__dirname, '../dist'))
    );
});

gulp.task('watch', () => {
  gulp.watch([ path.join(__dirname, '*.ts')], ['build']);
});
