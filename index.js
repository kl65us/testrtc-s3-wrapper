"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var AWS = require('aws-sdk');
var stream = require("stream");
var intoStream = require("into-stream");
var fs = require("fs");
var S3Storage = /** @class */ (function () {
    function S3Storage(creds, params) {
        if (params === void 0) { params = {}; }
        this.BUCKET = 'rtctmp';
        this._params = {};
        var _defaultConfig = {
            signatureVersion: 'v4',
            maxRetries: 3
        };
        // INFO: Bucket, Key
        this._params = params;
        if (!creds) {
            throw new Error("No credentials for S3 instance");
        }
        var _s3Params = Object.assign({}, _defaultConfig, {
            accessKeyId: creds.accessKeyId,
            secretAccessKey: creds.secretAccessKey,
            region: creds.region
        });
        // presave params
        this._awsParams = _s3Params;
        this._s3 = new AWS.S3(_s3Params);
    }
    S3Storage.prototype.trim = function (name) {
        var _name = name.replace(/\//g, '__');
        _name = _name.replace(':', '-');
        return _name;
    };
    S3Storage.prototype.prepareName = function (name) {
        var _name;
        if (name.startsWith('https://') || name.startsWith('http://')) {
            var pos = name.indexOf(this._params.Bucket + "/");
            if (pos >= 0) {
                _name = name.substring(pos + (this._params.Bucket.length + 1));
            }
            else {
                _name = name;
            }
        }
        else {
            _name = name;
        }
        return this.trim(_name);
    };
    S3Storage.prototype.save = function (name, readStream) {
        var _name = name.replace(/\//g, '__');
        _name = _name.replace(':', '-');
        var _params = Object.assign({}, this._params, { Key: _name, Body: readStream, ACL: 'public-read' });
        return this._s3.putObject(_params).promise();
    };
    S3Storage.prototype.read = function (name, writeStream) {
        // let _name = name.replace(/\//g, '__');
        // _name = _name.replace(':', '-');
        var _name = this.prepareName(name);
        var _params = Object.assign({}, this._params, { Key: _name });
        return this._s3.getObject(_params).promise().then(function (obj) {
            return new Promise(function (resolve, reject) {
                intoStream(obj.Body)
                    .pipe(writeStream)
                    .on('finish', resolve)
                    .on('error', reject);
            });
        });
    };
    S3Storage.prototype["delete"] = function (name) {
        // const _name = name.replace(/\//g, '__');
        var _name = this.prepareName(name);
        var _params = Object.assign({}, this._params, { Key: _name });
        return this._s3.deleteObject(_params).promise();
    };
    S3Storage.prototype.readAsObject = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _name, _params;
            return __generator(this, function (_a) {
                _name = this.prepareName(name);
                _params = Object.assign({}, this._params, { Key: _name });
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        return _this.returnBuffer(_this._s3.getObject(_params)).then(function (obj) {
                            var _json = obj.toString('utf8');
                            return resolve(JSON.parse(_json));
                        }, function (err) { return reject(err); });
                    })];
            });
        });
    };
    S3Storage.prototype.readAsBuffer = function (name) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _name, _params;
            return __generator(this, function (_a) {
                _name = this.prepareName(name);
                _params = Object.assign({}, this._params, { Key: _name });
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        return _this.returnBuffer(_this._s3.getObject(_params)).then(function (buff) {
                            return resolve(buff);
                        }, function (err) { return reject(err); });
                    })];
            });
        });
    };
    ;
    S3Storage.prototype.returnBuffer = function (stream) {
        return __awaiter(this, void 0, void 0, function () {
            var _chunks;
            return __generator(this, function (_a) {
                _chunks = [];
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        stream.createReadStream()
                            .on('data', function (data) {
                            _chunks.push(data);
                        })
                            .on('end', function () {
                            return resolve(Buffer.concat(_chunks));
                        });
                    })];
            });
        });
    };
    S3Storage.prototype.list = function (prefix) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _prefix, _params;
            return __generator(this, function (_a) {
                _prefix = prefix.replace(/\//g, '__');
                _params = Object.assign({}, this._params, { Prefix: _prefix });
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this._s3.listObjectsV2(_params).promise().then(function (listOfFiles) {
                            var _preparedFiles = listOfFiles.Contents.map(function (file) {
                                var _properties = {};
                                for (var prop in file) {
                                    if (Object.prototype.hasOwnProperty.call(file, prop)) {
                                        if (prop !== 'Key') {
                                            _properties[prop] = file[prop];
                                        }
                                    }
                                }
                                return {
                                    fullBlobName: "https://s3." + _this._awsParams.region + ".amazonaws.com/" + _params.Bucket + "/" + file.Key,
                                    properties: _properties,
                                    metadata: { MaxKeys: listOfFiles.MaxKeys, Prefix: listOfFiles.Prefix, Name: listOfFiles.Name }
                                };
                            });
                            return resolve(_preparedFiles);
                        }, function (err) { return reject(err); });
                    })];
            });
        });
    };
    S3Storage.prototype.transformToStream = function (data) {
        if (data instanceof stream.Readable) {
            return data;
        }
        else if (Buffer.isBuffer(data)) {
            return this.convertBufferToStream(data);
        }
        else if (data instanceof Object) {
            return this.convertObjectIntoStream(data);
        }
        else if (typeof data === "string") {
            return fs.createReadStream(data);
        }
        else {
            throw new Error("Specified parameter has unsupported type.");
        }
    };
    S3Storage.prototype.convertBufferToStream = function (buffer) {
        return intoStream(buffer);
    };
    S3Storage.prototype.convertObjectIntoStream = function (object) {
        var stringifiedJson = JSON.stringify(object, null, 0);
        var buffer = new Buffer(stringifiedJson, "utf8");
        return this.convertBufferToStream(buffer);
    };
    return S3Storage;
}());
exports.S3Storage = S3Storage;
