"use strict";

const path = require('path');
const fs = require('fs');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
chai.should();

chai.use(chaiAsPromised);

const Uploader = require(path.join(__dirname, '../', 'dist')).S3Storage;
const _fileName = `test${Math.random() + 1}`;
const _paramsUpload = {
  Bucket: 'rtctmp',
  Key: _fileName,
  // all we send should be a stream
  Body: fs.createReadStream(path.join(__dirname, 'fixtures', 'dump_data'))
};
const _paramsToMan = {
  Bucket: 'rtctmp',
  Key: _fileName,
};

describe('s3-wrapper test', () => {
  const creds = {
    // INFO: put here your keys to make test working
    accessKeyId: 'AKIAJRQ2WK4BLV6PI3RA',
    secretAccessKey: 'dHHmAw0/HPPiAY8qM1KdvjALHMzIGbB1ciabvXFp',
    region: 'eu-central-1'
  };
  const params ={
    Bucket: 'rtctmp'
  };
  const s3 = new Uploader(creds, params);

  describe('send, download and delete files', () => {
    let uploadedFileURL;

    it('should send a random file to S3 bucket', () => {
      // options related specifically to operation like
      // size of file, how many tries etc
      // params relate to bucket name, title of a file etc
      const _opts = {};
      const fname = path.join(__dirname, 'fixtures', 'dump_data');
      console.log(`fname: ${fname} ; type is ${typeof fname}`);
      return s3.save( _fileName, fs.createReadStream(path.join(__dirname, 'fixtures', 'dump_data')));
    });

    it('should download a file from S3 bucket', done => {
      s3.read(_paramsToMan).then( data => {
        expect(data).to.have.property('Body');
        const downloadedData = data.Body.toString();
        expect(downloadedData).include('s3');
        done();
      });
    });

    it('should delete a file', done => {
      // first delete an object
      s3.delete(_paramsToMan).should.be.fulfilled.then( data => {
        // and then check if you can download it
        return s3.read(_paramsToMan).should.be.rejected.and.notify(done);
      });
    });


  });

  describe(' read files as object, buffer and return list of files', () => {

    const _fileName = `test${Math.random() + 1}`;
    const _paramsUpload = {
      Bucket: 'rtctmp',
      Key: _fileName,
      Body: fs.createReadStream(path.join(__dirname, 'fixtures', 'test_object.json'))
    };
    const _paramsToMan = {
      Bucket: 'rtctmp',
      Key: _fileName,
    };
    const _params = Object.assign({}, _paramsToMan, {  })

    it('should read file as object', done => {
      //before we need to upload some object to AWS
      s3.save( _paramsUpload, {} ).should.be.fulfilled.then( resp => {
        s3.readAsObject(_paramsToMan).then( data => {
          expect(data).to.have.property('some');
          done();
        });
      });
    });

    it('should read file as buffer', done => {
      s3.readAsBuffer(_paramsToMan).should.be.fulfilled.then( data => {
        expect(Buffer.isBuffer(data)).to.be.true;
        done();
      });
    });

    it('should return list of files', done => {
      const _params = {
        Bucket: _paramsToMan.Bucket,
        MaxKeys: 500
      };

      s3.list(_params).should.be.fulfilled.and.then( files => {
        expect(files).to.be.an('array');
        expect(files).to.have.lengthOf.above(0);
        done();
      });
    });

    it('should delete created files', done => {
      s3.delete(_paramsToMan).should.be.fulfilled.then( data => {
        // and then check if you can download it
        return s3.read(_paramsToMan).should.be.rejected.and.notify(done);
      });
    });

  });
});


